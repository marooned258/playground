"""Save frames from video input."""
import os

import cv2
import argparse
import tqdm


def parse_arguments():
    parser = argparse.ArgumentParser(description="Save frames from video input.")

    parser.add_argument("-ip", "--input_path", required=True, help="Input path for video file.")
    parser.add_argument("-op", "--output_path", default="./vid/frames", help="Output path for saved frames.")

    args = vars(parser.parse_args())

    return args


def capture_video(input_path, output_path):
    capture = cv2.VideoCapture(input_path)

    frame_num = 1  # initial frame value

    pbar = tqdm.tqdm()

    while capture.isOpened():
        pbar.update(1)

        # frame by frame
        check, frame = capture.read()

        # if frame is ok, ret is True
        if not check:
            print("Exiting...")
            break

        # save every frame with different name
        cv2.imwrite(output_path + "/f_" + str(frame_num).zfill(2) + ".jpg", frame)

        frame_num += 1  # increment frame number

    # everything is done, close Camera
    capture.release()
    cv2.destroyAllWindows()

    pbar.close()


if __name__ == "__main__":
    args = parse_arguments()

    capture_video(args["input_path"], args["output_path"])
