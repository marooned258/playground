import cv2


def draw():
    img = cv2.imread("img/1.jpg", 1)  #read imeage

    dimensions = img.shape  # get dimensions of image

    print("Image dimensions are:\n\tImage widht: {}\n\tImage height: {}".format(dimensions[1], dimensions[0]))

    #draw rectangle over image
    cv2.rectangle(img, (1300, 800), (2400, 2100), (255, 255, 255), 15)

    #draw circle inside the rectable
    cv2.circle(img, (1850, 1450), 550, (255, 255, 0), 20)

    #draw lines from corned to corner of square
    #upper left corner
    cv2.line(img, (100, 100), (1300, 800), (0, 255, 0), 10)

    #upper right corner
    cv2.line(img, (3900, 100), (2400, 800), (0, 255, 0), 10)

    #bottom left corner
    cv2.line(img, (100, 2567), (1300, 2100), (0, 255, 0), 10)

    #bottom right corner
    cv2.line(img, (3900, 2567), (2400, 2100), (0, 255, 0), 10)

    cv2.namedWindow("myImg", cv2.WINDOW_NORMAL)  #resize imeage
    cv2.imshow("myImg", img)  # open/show imeage

    k = cv2.waitKey(0) & 0xFF  # prss any button to close

    if k == ord("s"):  # press 's' to save and exit
        cv2.imwrite("img/result.jpg", img)  # save image with different name
        cv2.destroyAllWindows()  # close window

        print("Done. Image saved and closed.")
    else:
        cv2.destroyAllWindows()  # close window

        print("Done. Image closed.")


if __name__ == "__main__":
    draw()
