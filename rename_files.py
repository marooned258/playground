"""Rename filenames for all the files in a specific folder."""

import os
import argparse


# Arg_parse for the input path of the folder with files
def parse_arguments():
    parser = argparse.ArgumentParser(description="Rename filename from folder.")

    parser.add_argument("-ip", "--input_path", required=True, help="Input path for the folder with files to rename.")

    args = vars(parser.parse_args())

    return args


def main(input_path):
    # If the input path is not valid the script stops and return a message
    if not os.path.exists(input_path):
        print("Invalid input path.")
        return False

    # create a variable with a list with all the filenames from the specific directory
    files = os.listdir(input_path)

    for i in range(len(files)):
        src = input_path + "/" + files[i]

        files[i] = files[i][:-4].split("_")
        files[i][0] = "frame"
        files[i][1] = files[i][1].zfill(2)

        dst = input_path + "/" + "_".join(files[i]) + ".jpg"

        os.rename(src, dst)


if __name__ == "__main__":
    args = parse_arguments()

    main(args["input_path"])
