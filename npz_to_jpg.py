import os

import cv2
import numpy as np

from tqdm import tqdm
import argparse


def parse_arguments():
    parser = argparse.ArgumentParser(description="Save frames from video input.")

    parser.add_argument("-ip", "--input_path", required=True,
                        help="Input path for data set directory with npz`s.")
    parser.add_argument("-op", "--output_path", default="/home/alin/bitbucket/playground/npz_jpg_output/",
                        help="Output path all for saved frames.")

    args = vars(parser.parse_args())

    return args


def getListOfFiles(dirPath):
    listFiles = list()

    for dirpath, dirnames, filenames in os.walk(dirPath):
        listFiles += [os.path.join(dirpath, file) for file in filenames]

    return listFiles


def main(input_path, output_path):
    allFiles = getListOfFiles(input_path)

    for file_path in tqdm(allFiles):
        with np.load(file_path) as data:
            colorImages = data["colorImages"]

        frame_number = 1
        current_filename = file_path.split('\\')[-1][0:-4]

        for i in range(colorImages.shape[3]):
            img = colorImages[:, :, :, i][:, :, ::-1]

            # save every frame with different name
            cv2.imwrite(output_path + "\\" + current_filename + "_" + str(frame_number).zfill(2) + ".jpg", img)

            frame_number += 1


if __name__ == "__main__":
    args = parse_arguments()

    main(args["input_path"], args["output_path"])
