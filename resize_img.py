import os

import argparse
import cv2


def argumet_parse():
    parser = argparse.ArgumentParser(description="Resize image w & h but keep aspect ratio")

    parser.add_argument("-ip", "--input_path", required=True, help="Input path for image")
    parser.add_argument("-x", "--x_axis", type=int, default=100, help="Input point coordinate for X axis (width)")
    parser.add_argument("-y", "--y_axis", type=int, default=100, help="Input point coordinate for Y axis (height)")
    parser.add_argument("-nw", "--new_width", type=int, default=1000, help="Input new width for resized image")
    parser.add_argument("-nh", "--new_height", type=int, default=1000, help="Input new height for resized image")

    args = vars(parser.parse_args())

    return args


def main(input_path, x_axis, y_axis, new_width, new_height):
    # Check if input path is valid
    if not os.path.exists(input_path):
        print("Invalid image path.")
        return False

    # Read the image
    img = cv2.imread(input_path, 1)

    # calculate the percentages for x and y
    x_per = 100 / img.shape[1] * x_axis
    y_per = 100 / img.shape[0] * y_axis

    dimensions = (new_width, new_height)  # (width, height)
    resize_img = cv2.resize(img, dimensions)

    x_new = x_per/100 * dimensions[0]
    y_new = y_per/100 * dimensions[1]

    cv2.circle(resize_img, (round(x_new), round(y_new)), 20, (0, 0, 0), -1)

    print("Original img size (h, w):", img.shape)
    print("Resized img (h, w):", resize_img.shape)

    print("Initial point coordinates (x,y): ", (x_axis, y_axis))
    print("New coordinates position (x,y): ", (round(x_new), round(y_new)))


    # show image, press any key to close
    cv2.namedWindow("img", cv2.WINDOW_NORMAL)
    cv2.imshow("img", resize_img)
    cv2.waitKey(0) & 0xFF
    cv2.destroyAllWindows()


if __name__ == "__main__":
    args = argumet_parse()
    main(args["input_path"], args["x_axis"], args["y_axis"], args["new_width"], args["new_height"])
